﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ObtenerTiempoRecord : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Text texto = this.GetComponent<Text>();

        float mayorPuntajeGuardado = PlayerPrefs.GetFloat("RECORD_TIME", 0); ;
        TimeSpan recordSpan = TimeSpan.FromSeconds(mayorPuntajeGuardado);
        texto.text = recordSpan.Hours.ToString("00") + ":" + recordSpan.Minutes.ToString("00") + ":" + recordSpan.Seconds.ToString("00");
    }
}
