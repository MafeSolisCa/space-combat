﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    private float tiempoDuraEnPantall = 3f;
    private float tiempoHastaMonento;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        tiempoHastaMonento += Time.deltaTime;
        if (tiempoHastaMonento > tiempoDuraEnPantall)
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
