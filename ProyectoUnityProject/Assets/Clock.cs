﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    [Tooltip("Tiempo inicial en segundos")]
    public int tiempoInicial;
    [Tooltip("Escala del tiempo del reloj")]
    [Range(0f,1000000000.00f)]
    public float escalaDeTiempo=1;
    public float mayorPuntaje;

    public Text tiempoText;
    public Text recordText;
    private float tiempoDelFrameConTiempo = 0f;
    private float TiempoAMostrarEnSegundos = 0f;
    private float escalaTiempoPausa,escalaTiempoInicio;
    private bool estaPausado = false;

    // Start is called before the first frame update
    void Start()
    {
        //establece la escala original de tiempo
        escalaTiempoInicio = escalaDeTiempo;

        //acomula el tiempo
        TiempoAMostrarEnSegundos = escalaTiempoInicio;

        ActualizarReloj(tiempoInicial);

        mayorPuntaje = PlayerPrefs.GetFloat("RECORD_TIME", 0); ;
        float mayorPuntajeGuardado = mayorPuntaje;
        TimeSpan recordSpan = TimeSpan.FromSeconds(mayorPuntajeGuardado);
        recordText.text = recordSpan.Hours.ToString("00") + ":" + recordSpan.Minutes.ToString("00") + ":" + recordSpan.Seconds.ToString("00");
    }

    public void ActualizarReloj(float tiempoEnSegundos)
    {
        
        string textoDelReloj;
        if (mayorPuntaje< tiempoEnSegundos)
        {
            mayorPuntaje = tiempoEnSegundos;
            PlayerPrefs.SetFloat("RECORD_TIME",mayorPuntaje);

            TimeSpan recordSpan = TimeSpan.FromSeconds(mayorPuntaje);

            recordText.text = recordSpan.Hours.ToString("00") + ":" + recordSpan.Minutes.ToString("00") + ":" + recordSpan.Seconds.ToString("00");
        }
        if (tiempoEnSegundos < 0)
        {
            tiempoEnSegundos = 0;
        }

        TimeSpan span = TimeSpan.FromSeconds(tiempoEnSegundos);

        textoDelReloj = span.Hours.ToString("00") + ":" + span.Minutes.ToString("00") + ":" + span.Seconds.ToString("00");

        tiempoText.text = textoDelReloj;
    }

    // Update is called once per frame
    void Update()
    { 
       
        if (!estaPausado) { 
        tiempoDelFrameConTiempo = Time.deltaTime * escalaDeTiempo;
        TiempoAMostrarEnSegundos += tiempoDelFrameConTiempo;
        ActualizarReloj(TiempoAMostrarEnSegundos);
    }
    }
    public void Pusar()
    {
        if (estaPausado)
        {
            estaPausado = true;
            escalaTiempoPausa = escalaDeTiempo;
            escalaDeTiempo = 0;
        }
    }

    public void Continuar()
    {
        if (estaPausado)
        {
            estaPausado = false;
            escalaDeTiempo = escalaTiempoPausa;
        }
    }

    public void Reiniciar()
    {
        estaPausado = false;
        escalaDeTiempo = escalaTiempoInicio;
        TiempoAMostrarEnSegundos = tiempoInicial;
        ActualizarReloj(TiempoAMostrarEnSegundos);
    }
}
