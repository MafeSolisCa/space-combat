﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move3Ene : MonoBehaviour
{
    public Vector3 direccion;
    public float speed;
    public float rotacion;
    public Rigidbody2D rigid2D;
    public float updateCadaCuanto=5f;
    public float cuanto=5f;
    

    void Update()
    {
        cuanto += Time.deltaTime; 
        if (cuanto> updateCadaCuanto)
        {
        speed = Random.Range(-1f, -2f);
        rotacion = Random.Range(-10f, 10f);
            cuanto = 0;
        }
        
        direccion = new Vector3(rotacion,speed,0f);
        rigid2D.velocity = direccion;
    }
}
