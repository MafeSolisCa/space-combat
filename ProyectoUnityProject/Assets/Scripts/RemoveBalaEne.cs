﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveBalaEne : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "bala2Ene(Clone)" || collision.gameObject.name == "bala(Clone)" || collision.gameObject.name == "bala4Ene(Clone)")
        {
            Destroy(collision.gameObject);
        }

    }
}
