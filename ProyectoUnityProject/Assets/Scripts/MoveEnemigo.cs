﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemigo : MonoBehaviour
{
    public Vector3 direccion = new Vector3(6f, -1f, 0f);
    public float cadaCuanto = 5f;
    private float _tiempoHastaElMomento = 5f;
    public Rigidbody2D rigid2D;
    void Update()
    {
        _tiempoHastaElMomento += Time.deltaTime;

        if (_tiempoHastaElMomento > cadaCuanto)
        {
            direccion.x = direccion.x * -1;

            _tiempoHastaElMomento -= cadaCuanto;
            rigid2D.velocity = direccion;
        }
    }
}
