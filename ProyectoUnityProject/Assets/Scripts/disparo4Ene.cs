﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparo4Ene : MonoBehaviour
{
    public GameObject prefab;
    public move4Ene instanceEne;
    public static BichitoMove ins;
 

    public void crearbala()
    {

        GameObject cube = GameObject.Instantiate(prefab);
        cube.transform.position = instanceEne.transform.position;
        cube.transform.position += new Vector3(0f, -3f, 0f);
        Rigidbody2D cubitoRigidbody = cube.GetComponent<Rigidbody2D>();
        cubitoRigidbody.velocity = BichitoMove.Instance.transform.position - instanceEne.transform.position;

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            crearbala();
        }

    }
}
