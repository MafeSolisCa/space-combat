﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pisoAbajo : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "enemigo(Clone)" || collision.gameObject.name == "enemigo2(Clone)" || collision.gameObject.name == "enemigo3(Clone)" || collision.gameObject.name == "enemigo4(Clone)")
        {
            SceneManager.LoadScene("GameOver");
        }

    }
}
