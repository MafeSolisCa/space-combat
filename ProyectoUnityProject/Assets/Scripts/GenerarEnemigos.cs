﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerarEnemigos : MonoBehaviour
{
    public GameObject prefab;
    public float tiempo = 7f;
    public Transform p;//es la posicion

    public void crearEnemigo1()
    {      
        GameObject cube = GameObject.Instantiate(prefab);
        cube.transform.position= p.transform.position - new Vector3(0f,0f,1f);
    }
    void Update()
    {
        tiempo += Time.deltaTime;       
        if(tiempo > 8f)
        {
            crearEnemigo1();
            tiempo = 0f;
        }

    }
}
