﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move4Ene : MonoBehaviour
{
    public Vector3 direccion;
    public float speed;
    public float rotacion;
    public Rigidbody2D rigid2D;
    public float updateCadaCuanto = 2f;
    public float cuanto = 5f;


    void Update()
    {
        cuanto += Time.deltaTime;
        if (cuanto > updateCadaCuanto)
        {
            speed = Random.Range(-1.5f, -0.5f);
            rotacion = Random.Range(-10f, 10f);
            cuanto = 0;
        }

        direccion = new Vector3(rotacion, speed, 0f);
        rigid2D.velocity = direccion;
    }
}
