﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BichitoMove : MonoBehaviour
{
    public float velocidad = 8f;
    public Animator anim;
    private static BichitoMove _instance;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    public static BichitoMove Instance
    {
        get => _instance;
    }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {

        Vector3 mov = new Vector3(
            Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical"),
            0);
        transform.position = Vector3.MoveTowards(
            transform.position, transform.position + mov, velocidad * Time.deltaTime);
        anim.SetFloat("MovX", mov.x);
        anim.SetFloat("MovY", mov.y);
      
    }
}
