﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public GameObject prefab;
    public BichitoMove bm;
    public float tiempoMomento = 5f;
    public static aumentoVelocity av;

    public void crearcubo()
    {
        
        GameObject cube = GameObject.Instantiate(prefab);
        cube.GetComponent<Desaparesco>().tiempoVida = tiempoMomento;
        cube.transform.position = bm.transform.position;
        cube.transform.position += new Vector3(0f, 3f, 0f);
        Rigidbody2D cubitoRigidbody = cube.GetComponent<Rigidbody2D>();
        cubitoRigidbody.velocity = new Vector3(0f, 6f, 0f) + aumentoVelocity.Instancee.CuantoVoyAumentando;
       
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            crearcubo();
           
        }
       
    }
}
