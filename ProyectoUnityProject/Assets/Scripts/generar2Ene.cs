﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generar2Ene : MonoBehaviour
{
    public GameObject prefab;
    public float tiempo = 0f;
    public Transform pos;

    public void crearEnemigo1()
    {

        GameObject cube = GameObject.Instantiate(prefab);
        cube.transform.position = pos.transform.position - new Vector3(0f, 0f, 1f);
    }
    void Update()
    {
        tiempo += Time.deltaTime;
        if (tiempo > 30f)
        {
            crearEnemigo1();
            tiempo = 0f;
        }

    }
}
