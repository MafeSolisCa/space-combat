﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desaparesco : MonoBehaviour
{
    public float tiempoVida = 5f;
    private float cuantoVivido = 0f;

    private void Update()
    {
        cuantoVivido += Time.deltaTime;
        if (cuantoVivido> tiempoVida)
        {

            Destroy(this.gameObject);
        }
    }
}
