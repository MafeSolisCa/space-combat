﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dispara2Ene : MonoBehaviour
{
    public GameObject prefab;
    public move2Ene me;
    public static BichitoMove ins;
    
    public void crearbala()
    {

        GameObject cube = GameObject.Instantiate(prefab);
        cube.transform.position = me.transform.position;
        cube.transform.position += new Vector3(0f, -3f, 0f);
        Rigidbody2D cubitoRigidbody = cube.GetComponent<Rigidbody2D>();
        cubitoRigidbody.velocity = BichitoMove.Instance.transform.position - me.transform.position;

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            crearbala();
        }
        
    }
}