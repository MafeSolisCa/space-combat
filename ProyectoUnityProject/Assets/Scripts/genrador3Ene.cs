﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class genrador3Ene : MonoBehaviour
{
    public GameObject prefab;
    public float tiempo = 0f;
    public Transform p;

    public void crearEne3()
    {
        GameObject ene3 = (GameObject)Instantiate(prefab);
        ene3.transform.position = p.transform.position - new Vector3(0f, 0f, 1f);
    }
    void Update()
    {
        tiempo += Time.deltaTime;
        if (tiempo > 50f)
        {
            crearEne3();
            tiempo = 0f;
        }

    }
}
